#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <unistd.h>
#include <string.h>

/********************* TP SIGNAUX *********************/
// Exercice 1
int main(int argc, char *argv[])
{
    int tube1[2], tube2[2], tret[2]; // tret : Tube retour
    pid_t idf1, idf2;
    int i, x;
    int nb;
    float moyenne;

    char buf[80]; // buf : Buffer dans lequel on va lire dans le fils

    pipe(tube1);
    pipe(tube2);

    idf1 = fork();

    if (idf1 > 0)
        idf2 = fork();

    if (idf1 > 0 && idf2 > 0)
    { // PERE
        close(tube1[0]);
        close(tube2[0]);
        close(tret[1]);

        for (i = 1; i < argc; i++)
        {
            x = atoi(argv[i]);
            if (x > 0)
                write(tube1[1], &x, sizeof(int));
            if (x < 0)
                write(tube2[1], &x, sizeof(int));
        }

        close(tube1[1]);
        close(tube2[1]);

        while (read(tret[0], buf, 80) > 0)
            printf("%s \n", buf);

        printf("Fin père\n");
        close(tret[0]);
    }
    else
    {
        if (idf1 == 0)
        { // 1ER FILS
            close(tube1[1]);
            close(tube2[0]);
            close(tube2[1]);
            close(tret[0]);

            moyenne = 0; 
            nb = 0; 

            while (read(tube2[0], &x, sizeof(int)) > 0) {
                moyenne += x; // pour l'instant c'est la même 
                nb++;
            }

            close(tube2[0]); 
            moyenne /= nb; 

            sprintf(buf, " La moyenne de %d entiers pos. reçus : %f\n", nb, moyenne); 
            write(tret[1], buf, strlen(buf) + 1); 
            close(tret[1]);
        }
        else {
            // 2EME FILS
            close(tube1[0]);
            close(tube1[1]);
            close(tube2[1]);
            close(tret[0]);

            moyenne = 0; 
            nb = 0; 

            while (read(tube2[0], &x, sizeof(int)) > 0) {
                moyenne += x; // pour l'instant c'est la moyenne
                nb++;
            }

            close(tube2[0]); 
            moyenne /= nb; 

            sprintf(buf, " La moyenne de %d entiers pos. reçus : %f\n", nb, moyenne); 
            write(tret[1], buf, strlen(buf) + 1); 
            close(tret[1]);
        }

    }
    return 0;
}