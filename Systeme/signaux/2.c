#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>

/********************* TP SIGNAUX *********************/
// Exercice 1
int main(int argc, char *argv[])
{
    int tube[2];
    pid_t idf;
    int i, x;
    int nb;
    float moyenne;

    pipe(tube);

    idf = fork();

    if (idf > 0)
    {
        // PERE
        close(tube[0]);
        for (i = 1; i < argc; i++)
        {
            x = atoi(argv[i]);
            if (x > 0)
                write(tube[1], &x, sizeof(int));
        }
        close(tube[1]);
        wait(NULL);
        printf("Fin père \n");
    }
    else
    {
        // FILS
        close(tube[1]);
        moyenne = 0;
        nb = 0;
        while (read(tube[0], &x, sizeof(int)) > 0)
        {
            moyenne += x;
            nb++;
        }
        close(tube[0]);
        moyenne /= nb;

        printf("La moyenne de %d entiers positifs reçus : %f\n", nb, moyenne);
    }

    return 0;
}