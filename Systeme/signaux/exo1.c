#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#define DUREE 5

int NbSigint, NbSigquit, NbSigterm, NbTotal;

/***************** Question 1.1 *****************/
void hand_protect(int sig)
{
    if (sig == SIGINT)
        NbSigint++;
    if (sig == SIGQUIT)
        NbSigquit++;
    if (sig == SIGTERM)
        NbSigterm++;

    NbTotal++;

    printf("SIG INT : %d  SIG QUIT : %d SIG TERM : %d  ||  Total : %d \n", NbSigint, NbSigquit, NbSigterm, NbTotal);

    if (NbTotal == 10)
    {
        printf("On en a assez, je quitte ! \n");
        exit(0);
    }
}

/***************** Question 1.2 *****************/

int main()
{
    int slrest, dureeTotale = 5;

    signal(SIGINT, signal(hand_protect()))

    printf("Identité du Processus : %d ", getpid());

    while (1)
    {
        slrest = DUREE;
        while (slrest > 0)
        {
            slrest = sleep(slrest);
        }
        dureeTotale += 5;
        printf("Duree totale de sommeil : %d \n", dureeTotale);
    }

    dureeTotale += 5;
}
