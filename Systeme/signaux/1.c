#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#define DUREE 5

int NbSigint, NbSigquit, NbSigterm, NbTotal;

/***************** Question 1.1 *****************/
void hand_protect(int sig)  
{
    if (sig == SIGINT)
        NbSigint++;
    /* 
    Question supplémentaire : 
    on veut qu'après avoir utilisé 3 SIGINT on appelle le handler par défaut qui est SIG_DFL (voir cours)*/
    if (NbSigint == 3) signal(SIGINT, SIG_DFL); // va tuer le processus car c'est le comportement par défaut
    if (sig == signal)
    if (sig == SIGQUIT)
        NbSigquit++;
    if (sig == SIGTERM)
        NbSigterm++;

    NbTotal++;

    printf("SIG INT : %d  SIG QUIT : %d SIG TERM : %d  ||  Total : %d \n", NbSigint, NbSigquit, NbSigterm, NbTotal);

    if (NbTotal == 10)
    {
        printf("On en a assez, je quitte ! \n");
        exit(0);
    }
}

/***************** Question 1.2 *****************/

int main()
{
    int slrest, dureeTotale = 5;

    signal(SIGINT, hand_protect);
    signal(SIGQUIT, hand_protect);
    signal(SIGTERM, hand_protect);
    signal(SIGPIPE, hand_protect);

    printf("Identité du Processus : %d\n", getpid());

    while (1)
    {
        slrest = DUREE;
        while (slrest > 0)
        {
            slrest = sleep(slrest);
        }
        dureeTotale += 5;
        printf("Duree totale de sommeil : %d \n", dureeTotale);
    }

    dureeTotale += 5;
}

/* 

Les commandes du terminal qu'il faut utiliser pour envoyer des signaux à notre programme : 

Ctrl + c : pour envoyer un SIGINT
kill -SIGINT [PID] : pour envoyer un SIGINT au programme avec terminal
kill [PID] : pour envoyer un SIGTERM au programme avec un terminal

Autres exemples : 
kill -SIGQUIT [PID]
kill -SIGTERM [PID]

Pour éxécuter le programme, on utilise la comande : 
gcc -Wall NOM_FICHIER -o NOM_EXECUTABLE

La version de Linux, ajoute toujours 128 pour le numéro de signal, et du coup la valeur de retour des programmes
tués par des SIGNAUX, c'est toujours 128 + n, où n est le numéro de signal

*/
