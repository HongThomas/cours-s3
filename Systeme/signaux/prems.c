#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>


void handler(int signal) {
	if (signal == SIGINT) {
		printf("NOPE !");
	}

}

int main() {
	printf("Test");	
	signal(SIGINT, handler);
	while(1) {
		printf("Id du processus : %d ", getpid());
		sleep(2);
	}
	return 0;
}

