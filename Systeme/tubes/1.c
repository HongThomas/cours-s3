#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#define NMAX 15
int main ( void )
{
       	int n , p[2] ;
	char c;
	if ( pipe ( p ) == -1 )
	{
	       	fprintf(stderr, "Erreur : tube \n");
	       	exit(1);
       	}
	if ( (n = fork( ) ) == -1 )
	{
	       	fprintf(stderr, "Erreur : fork \n");
	       	exit(2);
       	}
	if ( n )
	{ /* père */
		close(p[0]);
		while ( ( c = getchar() ) != EOF )
			if ( c >= 'a' && c<= 'z' ) write ( p[1] , &c, 1) ;
				close (p[1]);
			wait(0);
	}
	else
	{ /* fils */
		char chaine[NMAX];
		int i = 0;
		close ( p[1] );
		while ( read ( p[0], &chaine[i], 1) ) /* != 0 */
			if ( ++i == NMAX-1 ) break ;
		chaine[i] = '\0';
		printf(" Chaine = %s \n",chaine);
	}
}
