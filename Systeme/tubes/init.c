#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>

int main(){
	// Initialise le tube
	// tube[0] pour écrire
	// tube[1] pour lire
	int tube[2];
	
	// Gestion de l'erreur du tube
	if (pipe(tube) == -1) {
		printf("Erreur du tube !");
		return 0;
	}
	
	// Fork le processus pour créer un fils et faire communiquer le fils au père
	int proc = fork();

	// Dans le fils
	if (proc == 0) {
		close(tube[0]);
		int x;
		printf("Input a number : ");
		scanf("%d", &x);
		write(tube[1], &x, sizeof(int));
		close(tube[1]);
	}
	// Dans le père
	else {
		close(tube[1]);
		int y;
		read(tube[0], &y, sizeof(int));
		close(tube[0]);
		printf("Got from child process : %d \n ", y);
	}

	return 0;
}
